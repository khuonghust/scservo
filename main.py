from SCServo import SCServo
import serial.tools.list_ports
import time

def main():
	ports = list(serial.tools.list_ports.comports())
	for p in ports:
		if ("SCSERVO" in str(p)) or ("USB" in str(p)):
			temp = str(p).split(" -")
			SC = SCServo(temp[0],1000000)
			if SC.Connect():
				print ("Successfully connected to port %r. " % SC.GetPort())
			else:
				print ("Can't connect")
				exit()
	time.sleep(.5)
	ID = 4
	SC.EnableTorque(ID, 1)
	print("back to first position...")
	SC.WritePos(ID, 0, 10, Speed = 500)
	time.sleep(2)
	print ("running...")
	time.sleep(1)
	pos_new = 1000
	count = 0
	while True:
		count = count + 1
		SC.WritePos(ID, pos_new, 0, Speed = 500)
		while True:
			pos = SC.ReadPos(ID)
			speed = SC.ReadSpeed(ID)
			vol = SC.ReadVoltage(ID)/10
			temp = SC.ReadTemper(ID)
			load = SC.ReadLoad(ID)
			# goalpos = SC.ReadGoalPosition(ID)
			# moving = SC.ReadMoving(ID)
			print('pos:{:4}\tspeed:{:4}\tload:{:4}\t vol:{:4}\t temp:{:4}'.format(pos, speed, load, vol, temp))
			time.sleep(0.01)
			if abs(pos-pos_new)<=2:
				break
			
		print("done")
		SC.Reset(ID)
		time.sleep(2)
		if count % 2:
			pos_new = 20
		else:
			pos_new = 1000
if __name__ == '__main__':
	main()
	

		
		




