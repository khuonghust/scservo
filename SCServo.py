import serial
from struct import *

#----register Address
P_VERSION_L = 3
P_VERSION_H = 4
P_ID = 5
P_BAUD_RATE = 6
P_RETURN_DELAY_TIME = 7
P_RETURN_LEVEL = 8
P_MIN_ANGLE_LIMIT_L = 9
P_MIN_ANGLE_LIMIT_H = 10
P_MAX_ANGLE_LIMIT_L = 11
P_MAX_ANGLE_LIMIT_H = 12
P_LIMIT_TEMPERATURE = 13
P_MAX_LIMIT_VOLTAGE = 14
P_MIN_LIMIT_VOLTAGE = 15
P_MAX_TORQUE_L = 16
P_MAX_TORQUE_H = 17
P_ALARM_LED = 19
P_ALARM_SHUTDOWN = 20
P_COMPLIANCE_P = 21
P_PUNCH_L = 24
P_PUNCH_H = 25
P_CW_DEAD = 26
P_CCW_DEAD = 27

P_TORQUE_ENABLE = 40
P_LED = 41
P_GOAL_POSITION_L = 42
P_GOAL_POSITION_H = 43
P_GOAL_TIME_L = 44
P_GOAL_TIME_H = 45
P_GOAL_SPEED_L = 46
P_GOAL_SPEED_H = 47
P_LOCK = 48

P_PRESENT_POSITION_L = 56
P_PRESENT_POSITION_H = 57
P_PRESENT_SPEED_L = 58
P_PRESENT_SPEED_H = 59
P_PRESENT_LOAD_L = 60
P_PRESENT_LOAD_H = 61
P_PRESENT_VOLTAGE = 62
P_PRESENT_TEMPERATURE = 63
P_REGISTERED_INSTRUCTION = 64
P_MOVING = 66

# Instruction
INST_PING = 0x01
INST_READ = 0x02
INST_WRITE = 0x03
INST_REG_WRITE = 0x04
INST_ACTION = 0x05
INST_RESET = 0x06
INST_SYNC_WRITE = 0x83

class SCServo(object):
	"""Servo library for python"""
	def __init__(self, comport, baudrate):
		super(SCServo, self).__init__()
		self.comport = comport
		self.baudrate = baudrate
		self.ser = None

		self.End = 1
		self.Level = 1
	def Connect(self):
		try:
			if self.ser == None:
				self.ser = serial.Serial(self.comport, self.baudrate)
				if self.ser.isOpen():
					return True				
				else:
					return False
		except Exception as e:
			exit("Error connect")

	def Disconnect(self):
		try:
			if self.ser.isOpen():
				self.ser.close()
				return True
			else:
				return False
		except Exception as e:
			exit("Error disconnect")

	def GetPort(self):
		return self.ser.port

	def FlushSCS(self):
		self.ser.flush()


	def Host2SCS(self, Data):
		if (self.End):
			DataL = (Data>>8)
			DataH = (Data & 0xff)
		else:
			DataH = (Data>>8)
			DataL = (Data & 0xff)
		return DataL, DataH

	def SCS2Host(self, DataL, DataH):
		if self.End:
			Data = DataL + DataH
			Data = unpack('>H', Data)[0]
		else:
			Data = DataH + DataL
			Data = unpack('>H', Data)[0]
		return Data

	def WriteSCS(self, *args):
		if len(args) == 1:
			bDat = args[0]
			bDat = pack('B', bDat)
			return self.ser.write(bDat)
		elif len(args) == 2:
			nDat = args[0]
			nLen = args[1]
			if nDat is None:
				return False
			for i in range(nLen):
				nDat[i] = pack('B', nDat[i])
			for dat in nDat:
				self.ser.write(dat)

	def WriteBuf(self, ID, MemAddr, nDat, nLen, Fun):
		msgLen = 2
		bBuf=[0]*6
		CheckSum = 0
		bBuf[0] = 0xff
		bBuf[1] = 0xff
		bBuf[2] = ID
		bBuf[4] = Fun
		if (nDat):
			msgLen += nLen + 1
			bBuf[3] = msgLen
			bBuf[5] = MemAddr
			self.WriteSCS(bBuf,6)
		else:
			bBuf[3] = msgLen
			self.WriteSCS(bBuf,5)
		CheckSum = ID + msgLen + Fun + MemAddr
		if (nDat):
			for i in range(nLen):
				CheckSum += nDat[i]
		self.WriteSCS(nDat, nLen)
		self.WriteSCS(self.bit_not(CheckSum))

	def GenWrite(self, ID, MemAddr, nDat, nlen):
		self.FlushSCS()
		self.WriteBuf(ID, MemAddr, nDat, nLen, INST_WRITE)
		return self.Ack(ID)


	def RegWrite(self, ID, MemAddr, nDat, nLen):
		self.FlushSCS()
		self.WriteBuf(ID, MemAddr, nDat, nLen, INST_REG_WRITE)
		return self.Ack(ID)

	def SyncWrite(self, IDs, IDN, MemAddr, nDat, nLen):
		mesLen = (nLen + 1) * IDN + 4
		Sum = 0
		bBuf = [0]*7
		bBuf[0] = 0xff;
		bBuf[1] = 0xff;
		bBuf[2] = 0xfe;
		bBuf[3] = mesLen;
		bBuf[4] = INST_SYNC_WRITE;
		bBuf[5] = MemAddr;
		bBuf[6] = nLen;
		self.WriteSCS(bBuf, 7)

		Sum = 0xfe + mesLen + INST_SYNC_WRITE + MemAddr + nLen
		for i in range(IDN):
			self.WriteSCS(IDs[i])
			self.WriteSCS(nDat, nLen)
			Sum += IDS[i]
			for j in range(nLen):
				Sum += nDat[j]

		self.WriteSCS(self.bit_not(Sum))

	def WriteByte(self, ID, MemAddr, bDat):
		self.FlushSCS()
		self.WriteBuf(ID, MemAddr, [bDat], 1, INST_WRITE)
		return self.Ack(ID)

	def WriteWord(self, ID, MemAddr, wDat):
		self.FlushSCS()
		buf = [0]*2
		buf[0], buf[1] = self.Host2SCS(wDat)
		self.WriteBuf(ID, MemAddr, buf, 2, INST_WRITE)
		return self.Ack(ID)

	def EnableTorque(self, ID, Enable):
		self.WriteByte(ID, P_TORQUE_ENABLE, Enable)

	def writePos(self, ID, Position, Time, Speed = 0, Fun = None):
		self.FlushSCS()
		buf = [0]*6
		buf[0], buf[1] = self.Host2SCS(Position)
		buf[2], buf[3] = self.Host2SCS(Time)
		buf[4], buf[5] = self.Host2SCS(Speed)
		self.WriteBuf(ID, P_GOAL_POSITION_L, buf, 6, Fun)
		return self.Ack(ID)
	def WritePos(self, ID, Position, Time, Speed = 500):
		return self.writePos(ID, Position, Time, Speed, INST_WRITE)

	def RegWritePos(self, ID, Position, Time, Speed = 0):
		return self.WritePos(ID, Position, Time, Speed, INST_REG_WRITE)

	def RegWriteAction(self):
		self.WriteBuf(0xfe, 0, None, 0, INST_ACTION)

	def SyncWritePos(self, IDs, IDN, Position, Time, Speed = 0):
		buf = [0]*6
		buf[0], buf[1] = self.Host2SCS(Position)
		buf[2], buf[3] = self.Host2SCS(Time)
		buf[4], buf[5] = self.Host2SCS(Speed)
		self.SyncWrite(IDs, IDN, P_GOAL_POSITION_L, buf, 6)

	def WriteSpe(self, ID, Speed):
		if Speed<0:
			Speed = -Speed
			Speed |= (1<<10)
		return self.WriteWord(ID, P_GOAL_TIME_L, Speed)

	def readSerial(self):
		return self.ser.read()

	def ReadSCS(self, nLen):
		Size = 0
		ComData = None
		nDat = [0]*nLen
		while True:
			ComData =  self.readSerial()
			if ComData != -1:	
				if nDat:
					nDat[Size] = ComData
				Size = Size + 1
			if Size >= nLen:
				break
		return Size, nDat

	def Read(self, ID, MemAddr, nLen):
		self.FlushSCS()
		self.WriteBuf(ID, MemAddr, [nLen], 1, INST_READ)
		Size, bit12 = self.ReadSCS(2)
		if (bit12[0] != b'\xff') or (bit12[1] != b'\xff'):
			return 0, 0
		Size, bit3 = self.ReadSCS(1)
		Size, bit4 = self.ReadSCS(1)
		numb4 = unpack('<B', bit4[0])[0]
		Size, bits = self.ReadSCS(numb4)
		if Size:
			return Size, bits #tra ve cac bit cuoi cung (error+par+checksum)
		return 0, 0

	def ReadByte(self, ID, MemAddr):
		Size, bDat = self.Read(ID, MemAddr, 1)
		if Size != 3:
			return -1
		else:
			bDat.pop(0)
			bDat.pop(-1)
			return bDat[0]

	def ReadWord(self, ID, MemAddr):
		Size, nDat = self.Read(ID, MemAddr, 2)
		if Size != 4:
			return -1
		nDat.pop(0)
		nDat.pop(-1)
		return nDat

	def ReadPos(self, ID):
		nDat = self.ReadWord(ID, P_PRESENT_POSITION_L)
		wDat = self.SCS2Host(nDat[0], nDat[1])
		return wDat

	def ReadVoltage(self, ID):
		nData = self.ReadByte(ID, P_PRESENT_VOLTAGE)
		return unpack('<B', nData)[0]

	def ReadTemper(self, ID):
		nData = self.ReadByte(ID, P_PRESENT_TEMPERATURE)
		return unpack('<B', nData)[0]

	def ReadSpeed(self, ID):
		nDat = self.ReadWord(ID, P_PRESENT_SPEED_L)
		wDat = self.SCS2Host(nDat[0], nDat[1])
		bitDir = self.is_set(wDat, 15)
		if bitDir:
			return -(wDat & 0x7fff)
		else:
			return wDat
		return wDat

	def ReadMoving(self, ID):
		nData = self.ReadByte(ID, P_MOVING)
		return unpack('<B', nData)[0]

	def ReadGoalPosition(self, ID):
		nDat = self.ReadWord(ID, P_GOAL_POSITION_L)
		wDat = self.SCS2Host(nDat[0], nDat[1])
		return wDat

	def ReadLoad(self, ID):
		nDat = self.ReadWord(ID, P_PRESENT_LOAD_L)
		wDat = self.SCS2Host(nDat[0], nDat[1])
		bitDir = self.is_set(wDat, 10)
		if bitDir:
			return wDat & 0x03ff
		else:
			return -wDat
	def Ping(self, ID):
		self.FlushSCS()
		self.WriteBuf(ID, 0, None, 0, INST_PING);
		Size, bBuf = self.ReadSCS(6)
		if Size == 6:
			return bBuf[2]
		else:
			return -1

	def WheelMode(self, ID):
		bBuf = [0]*4
		return self.GenWrite(ID, P_MIN_ANGLE_LIMIT_L, bBuf, 4)

	def JoinMode(self, ID, minAngle = 0, maxAngle = 1023):
		buf = [0]*4
		buf[0], buf[1] = self.Host2SCS(minAngle)
		buf[2], buf[3] = self.Host2SCS(maxAngle)
		return self.GenWrite(ID, P_MIN_ANGLE_LIMIT_L, bBuf, 4)

	def Reset(self, ID):
		self.FlushSCS()
		self.WriteBuf(ID, 0, None, 0, INST_RESET);
		return self.Ack(ID)

	def Ack(self, ID):
		if ID != 0xfe and self.Level:
			Size, bBuf = self.ReadSCS(6)
			if Size != 6:
				return 0
		return 1

	def is_set(self, x, n):
		if x & 2**n != 0:
			return 1
		else:
			return 0

	def bit_not(self, n):
		return ~n & 0xff